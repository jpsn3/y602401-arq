# Arquitetura de Computadores

Repositório destinado aos códigos da disciplina de Arquitetura de Computadores (Y602401)

## Ementa

A ementa do curso está disponível no site [ALSM](https://gpads.recife.ifpe.edu.br/alsm/csin/index.php/arquitetura-de-computadores/)

## Conteúdo

O conteúdo é dividido em uma parte teória intercalada com parte teórica. O livro base da disciplina é o de [Guilherme Alvaro](http://compsim.crato.ifce.edu.br/book.html) disponível de forma gratuita.
Para parte prática será utilizado [CompSim - The Computer Simulator](http://compsim.crato.ifce.edu.br/download.html)

## Códigos

Os códigos serão colocados aqui conforme for passado o conteúdo de sala de aula.

## Benchmarks

O repositório de benchmarks utilizado na disciplina será o disponível no [GitLab](https://gitlab.com/alsmoreira/comp_arch)

## Códigos do CompSim está disponível para download na pasta cariri

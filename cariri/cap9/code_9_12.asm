;=================================
;File: code_9_12
;=================================

.code
	LDA var1         ;este bloco realiza: tmp = NAND(var1, var1).
	NAND var1        
	STA tmp

	LDA var2         ;este bloco realiza: AC = NAND(var2, var2).
	NAND var2        

	NAND tmp         ;realiza: NAND(AC, tmp).

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 0000000011111111b      ;int var1 = 0xffff.
	var2: DD 1111111100000000b      ;int var1 = 0xff00.
	tmp: DD 0
.stack 1
